package multiplicator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;

public class Main {

	private static PetriNet<String> petriNet;
	private static Collection<Transition<String>> calculatingTransitions = new ArrayList<>();
	private static List<Thread> threads = new ArrayList<>();

	private static class MyRunnable implements Runnable {

		private int x = 0;

		void answer() {
			System.out.print("Thread name: " + Thread.currentThread().getName()
					+ ". Fired " + x + " times.\n");
		}

		@Override
		public void run() {
			while(true) {
				if (Thread.currentThread().isInterrupted()) {
					answer();
					return;
				}
				try {
					petriNet.fire(calculatingTransitions);
					x++;
				} catch (InterruptedException e) {
					answer();
					return;
				}
			}
		}
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		Transition<String>
				t1 = new Transition<>(Map.of("B", 1, "A", 1), Set.of(), Set.of("T2"), Map.of("Sum", 1, "H", 1, "A", 1)),
				t2 = new Transition<>(Map.of("H", 1, "A", 1), Set.of(), Set.of("T1"), Map.of("Sum", 1, "B", 1, "A", 1)),
				t3 = new Transition<>(Map.of("T1", 1, "A", 1), Set.of(), Set.of("B"), Map.of("T2", 1)),
				t4 = new Transition<>(Map.of("T2", 1, "A", 1), Set.of(), Set.of("H"), Map.of("T1", 1)),
				lastTransition = new Transition<>(Map.of(), Set.of(), Set.of("A"), Map.of());

		calculatingTransitions.addAll(List.of(t1, t2, t3, t4));

		for (char x = 'A'; x <= 'D'; x++) {
			Thread thread = new Thread(new MyRunnable());
			thread.setName("Thread_" + x);
			threads.add(thread);
		}

		petriNet = new PetriNet<>(Map.of("T1", 1, "A", scan.nextInt(), "B", scan.nextInt()), true);

		for (Thread thread : threads)
			thread.start();

		try {
			petriNet.fire(Collections.singleton(lastTransition));
		} catch (InterruptedException e) {
			for (Thread thread : threads)
				thread.interrupt();
			return;
		}

		Map<String, Integer> result = petriNet.reachable(Collections.emptySet()).iterator().next();
		System.out.println(result.getOrDefault("Sum", 0));

		for (Thread thread : threads)
			thread.interrupt();
	}
}
