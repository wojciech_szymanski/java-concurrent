package alternator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;

public class Main {

    private static PetriNet<String> petriNet =
            new PetriNet<>(Map.of("Aready", 1, "Bready", 1, "Cready", 1), true);

    private static class MyRunnable implements Runnable {

        private Transition<String> firstTransition, secondTransition;

        MyRunnable(Transition<String> t1, Transition<String> t2) {
            this.firstTransition = t1;
            this.secondTransition = t2;
        }

        @Override
        public void run() {

            while(true) {
                if (Thread.currentThread().isInterrupted())
                    return;

                try {
                    petriNet.fire(Collections.singleton(firstTransition));
                } catch (InterruptedException e) {
                    return;
                }

                System.out.print(Thread.currentThread().getName());
                System.out.print(".");

                try {
                    petriNet.fire(Collections.singleton(secondTransition));
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    private static List<Thread> threads = new ArrayList<>();
    private static Collection<Transition<String>> allTransitions = new ArrayList<>();

    private static Collection<String> allPlacesNames = Arrays.asList("Aready", "Bready", "Cready", "Ain", "Bin", "Cin");
    private static Collection<String> inPlacesNames = Arrays.asList("Ain", "Bin", "Cin");

    private static boolean isSafe(Map<String, Integer> state) {
        int nodesIn = 0;
        for (String name : state.keySet()) {
            if (!allPlacesNames.contains(name)) {
                System.out.print("Name: " + name + " not in allPlacesNames\n");
                return false;
            }
            if (inPlacesNames.contains(name)) {
                nodesIn += state.get(name);
            }
        }
        if (nodesIn > 1) {
            System.out.print("To many threads in critical section\n");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {

        for (char x = 'A'; x <= 'C'; x++) {

            Map<String, Integer> input = Collections.singletonMap(x + "ready", 1);
            Map<String, Integer> output = Collections.singletonMap(x + "in", 1);

            Collection<String> reset = new ArrayList<>(allPlacesNames);
            Collection<String> inhibitor = new ArrayList<>(inPlacesNames);

            Map<String, Integer> secondOutput = new TreeMap<>();
            for (char y = 'A'; y <= 'C'; y++)
                if (x != y)
                    secondOutput.put(y + "ready", 1);

            Transition<String> przed = new Transition<>(input, Collections.emptyList(), inhibitor, output),
                    po = new Transition<>(output, reset, Collections.emptyList(), secondOutput);
            allTransitions.addAll(Arrays.asList(przed, po));

            MyRunnable myRunnable = new MyRunnable(przed, po);
            Thread thread = new Thread(myRunnable);
            thread.setName(String.valueOf(x));
            threads.add(thread);
        }

        Set<Map<String, Integer>> transitionsForSafetyCheck = petriNet.reachable(allTransitions);

        System.out.println("Number of reachable states: " + transitionsForSafetyCheck.size());

        for (Map<String, Integer> state : transitionsForSafetyCheck) {
            if (!isSafe(state)) {
                System.out.print("NOT SAFE");
                return;
            }
        }

        for (Thread thread : threads)
            thread.start();

        try {
            Thread.sleep(30000);
        } catch (InterruptedException ignore) {} finally {
            for (Thread thread : threads)
                thread.interrupt();
        }
    }
}
