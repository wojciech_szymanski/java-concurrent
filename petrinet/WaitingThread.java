package petrinet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Semaphore;

public class WaitingThread<T> {
	private Semaphore semaphore;
	private Collection<Transition<T>> transitions;
	private boolean isInterrupted;
	public WaitingThread(Collection<Transition<T>> transitions) {
		this.semaphore = new Semaphore(0);
		this.transitions = new ArrayList<>(transitions);
		isInterrupted = false;
	}

	public Semaphore getSemaphore() {
		return semaphore;
	}

	public Collection<Transition<T>> getTransitions() {
		return new ArrayList<>(transitions);
	}

	public boolean getInterrupted() {
		return isInterrupted;
	}

	public void interrupt() {
		isInterrupted = true;
	}

}
