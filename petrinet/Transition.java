package petrinet;

import java.util.*;

public class Transition<T> {

	private Map<T, Integer> input, output;
	private Collection<T> reset, inhibitor;

	public Transition(Map<T, Integer> input, Collection<T> reset, Collection<T> inhibitor, Map<T, Integer> output) {
		this.inhibitor = new TreeSet<>(inhibitor);
		this.input = new TreeMap<>(input);
		this.output = new TreeMap<>(output);
		this.reset = new TreeSet<>(reset);
	}

	public Collection<T> getReset() {
		return new ArrayList<>(reset);
	}

	public Map<T, Integer> getInput() {
		return new TreeMap<>(input);
	}

	public Map<T, Integer> getOutput() {
		return new TreeMap<>(output);
	}

	public boolean isEnabled(State<T> state) {

		boolean answer = true;

		for (T place : input.keySet()) {
			if (input.get(place) > state.getMarking(place)) {
				answer = false;
				break;
			}
		}

		for (T place : inhibitor) {
			if (!answer || state.getMarking(place) > 0) {
				answer = false;
				break;
			}
		}
		return answer;
	}
}