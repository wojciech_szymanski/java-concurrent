package petrinet;

import java.util.*;
import java.util.concurrent.Semaphore;

public class PetriNet<T> {

	private volatile State<T> mainState;

	private Semaphore mainSemaphore;

	public PetriNet(Map<T, Integer> initial, boolean fair) {
        mainSemaphore = new Semaphore(1, fair);
        this.mainState = new State<>(initial);
	}

	private List<WaitingThread<T>> waitingThreads = Collections.synchronizedList(new ArrayList<>());

	public Set<Map<T, Integer>> reachable(Collection<Transition<T>> transitions) {

		try {
			mainSemaphore.acquire();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			return new HashSet<>();
		}
		State<T> startState = mainState.getCopy();
		mainSemaphore.release();

        ArrayList<State<T>> queue = new ArrayList<>();
        Set<Map<T, Integer>> result = new HashSet<>();
        queue.add(startState);
        result.add(startState.getMap());

		for (int it = 0; it < queue.size(); it++) {
			for (Transition<T> transition : transitions) {
				if (transition.isEnabled(queue.get(it))) {
					State<T> newState = queue.get(it).apply(transition);
					if (!result.contains(newState.getMap())) {
						queue.add(newState);
						result.add(newState.getMap());
					}
				}
			}
		}

		return result;
	}

	private void checkWaitingThreads() {
		for (int it = 0; it < waitingThreads.size(); it++) {
			if (!waitingThreads.get(it).getInterrupted())
			for (Transition<T> transition : waitingThreads.get(it).getTransitions()) {
				if (transition.isEnabled(mainState)) {
					Semaphore semaphore =  waitingThreads.get(it).getSemaphore();
					waitingThreads.remove(it);
					semaphore.release();
					return;
				}
			}
		}
		mainSemaphore.release();
	}

	public Transition<T> fire(Collection<Transition<T>> transitions) throws InterruptedException {

		mainSemaphore.acquire();
		for (Transition<T> transition : transitions) {
			if (transition.isEnabled(mainState)) {
				mainState = mainState.apply(transition);
				checkWaitingThreads();
				return transition;
			}
		}

		WaitingThread<T> myWaitingThread = new WaitingThread<>(transitions);
		waitingThreads.add(myWaitingThread);
		mainSemaphore.release();
		try {
			myWaitingThread.getSemaphore().acquire();
		} catch(InterruptedException e) {
			myWaitingThread.interrupt();
			throw e;
		}

		Transition<T> result = null;
		for (Transition<T> transition : transitions) {
			if (transition.isEnabled(mainState)) {
				mainState = mainState.apply(transition);
				result = transition;
				break;
			}
		}
		assert(result != null);

		checkWaitingThreads();
		return result;
	}

}