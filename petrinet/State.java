package petrinet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class State<T> {

	private HashMap<T, Integer> map;

	public State(Map<T, Integer> map) {
		this.map = new HashMap<>(map);
	}

	public Integer getMarking(T t) {
		return map.getOrDefault(t, 0);
	}

	public Map<T, Integer> getMap() {
		return new HashMap<>(map);
	}

	public State<T> getCopy() {
		return this.apply(new Transition<>(new HashMap<>(), new HashSet<>(), new HashSet<>(), new HashMap<>()));
	}

	public State<T> apply(Transition<T> transition) {
		assert(transition.isEnabled(this));
		State<T> result = new State<>(map);
		for (T key : transition.getInput().keySet()) {
			Integer tokens = result.map.get(key);
			result.map.put(key, tokens - transition.getInput().get(key));
			if (result.getMarking(key).equals(0))
				result.map.remove(key);
		}
		for (T key : transition.getReset()) {
			result.map.remove(key);
		}
		for (T key : transition.getOutput().keySet()) {
			Integer tokens = result.getMarking(key);
			result.map.put(key, tokens + transition.getOutput().get(key));
		}
		return result;
	}
}
